/**
* Corral.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		nombre: { type: 'string', required: true },

		tipo: { type: 'string', enum: ['desarrollo', 'formacion'], required: true },

		descripcion: { type: 'text' },

		grupos: { collection: 'grupo', via: 'corral' },

		signos_corral: { collection: 'signo_corral', via: 'corral' },

		veterinario: { model: 'usuario' }
	},
	autoUpdatedAt: false
};

