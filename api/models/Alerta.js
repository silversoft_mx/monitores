/**
* Alerta.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		res: { model: 'res' },

		comentario: { type: 'string', defaultsTo: '' },

		estado: { type: 'string', required: true, defaultsTo: 'checar', enum: ['checado', 'checar', 'finalizado'] }
	}
};

