/**
* Res.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		arete_siniiga: { type: 'string', required: true },

		grupo: { model: 'grupo' },

		proveedor: { model: 'proveedor' },

		fecha_ingreso: { type: 'datetime' },

		raza: { model: 'raza' },

		edad_ingreso: { type: 'integer' },

		sexo: { type: 'string', enum: ['macho', 'hembra'] },

		fecha_salida: { type: 'datetime' },

		muerte: { type: 'boolean' },

		precio_compra: { type: 'string', required: true },

		precio_venta: { type: 'string' },

		aretes: { collection: 'arete', via: 'reses' },

		medidas: { collection: 'medida', via: 'res' },

		signos_vitales: { collection: 'signo_vital', via: 'res' },

		enfermedades: { collection: 'enfermedad_trans', via: 'res' },

		alertas: { collection: 'alerta', via: 'res' }
	},
	autoUpdatedAt: false
};

