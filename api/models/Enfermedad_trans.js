/**
* Enfermedad_trans.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		veterinario: { model: 'usuario' },

		res: { model: 'res' },

		enfermedad: { model: 'enfermedad' },

		problema: { type: 'string' },

		tipo_ganado: { type: 'string' },

		grado_enfermedad: { type: 'string', enum: ['1-Level', '2-Level', '3-Level', '4-Level', '5-Level'] },

		signos_enfermedad: { collection: 'signo_enfermedad', via: 'enfermedades' },

		fecha_inicio: { type: 'date', required: true },

		fecha_final: { type: 'date' },

		comentarios: { collection: 'comentario_enfermedad', via: 'enfermedad' },

		observaciones: { collection: 'comentario_enfermedad', via: 'enfermedad' },

		signos_extras: { collection: 'signo_extra', via: 'enfermedad_trans' },

		estado_enfermedad: { model: 'estado_enfermedad' },

		medicinas: { collection: 'medicina', via: 'enfermedad_trans' }
	},
	autoUpdatedAt: false
};

