/**
* Signo_corral.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		corral: { model: 'corral' },

		temperatura: { type: 'string', required: true },

		humedad: { type: 'string', required: true},

		fecha: { type: 'datetime', required: true }
	},
	autoUpdatedAt: false
};

