/**
* Signo_enfermedad.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		nombre: { type: 'string', required: true },

		activo: { type: 'boolean' },

		cuerpo_golpeado: { type: 'string' },

		enfermedades: { collection: 'enfermedad_trans', via: 'signos_enfermedad' }
	},
	autoUpdatedAt: false
};

