/**
* Proveedor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		nombre: { type: 'string', required: true, unique: true },

		telefono: { type: 'string' },

		ciudad: { model: 'ciudad' },

		descripcion: { type: 'string' },

		reses: { collection: 'res', via: 'proveedor' }
	},
	autoUpdatedAt: false
};

