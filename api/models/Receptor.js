/**
* Receptor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		folio: { type: 'string', required: true, unique: true },
		
		corral: { model: 'corral' }
	},
	autoUpdatedAt: false
};

