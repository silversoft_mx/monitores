/**
* Medida.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		res: { model: 'res' },

		peso: { type: 'string', required: true },

		altura: { type: 'string', required: true },

		fecha: { type: 'datetime', required: true }
	},
	autoUpdatedAt: false
};

