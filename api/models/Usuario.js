/**
* Usuario.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs				:: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt');

module.exports = {

	attributes: {
		nombre : { type: 'string', required: true },

		apellido : { type: 'string', required: true },

		correo : { type: 'email', unique: true, required: true },

		contrasena : { type: 'string', required: true, minLength: 5 },

		telefono : { type: 'string' },

		fecha_ingreso : { type: 'datetime' },

		rol: { model: 'rol', required: true },

		corrales: { collection: 'corral', via: 'veterinario' },

		reses_veterinario: { collection: 'enfermedad_trans', via: 'veterinario' },

		establo: { model: 'establo' }
	},
	autoUpdatedAt: false,

	beforeCreate: function (values, cb) {
		bcrypt.hash(values.contrasena, 10, function(err, hash) {
			if(err)
				return cb(err);
			values.contrasena = hash;
			cb();
		});
	}
};