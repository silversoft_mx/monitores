/**
* Formula_corral.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		grupo: { model: 'grupo' },

		formula: { model: 'formula', required: true },

		fecha_inicio: { type: 'date', required: true },

		fecha_final: { type: 'date' }
	},
	autoUpdatedAt: false
};

