/**
* Medicina.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		nombre: { type: 'string', required: true },

		precio: { type: 'string' },

		inventario: { type: 'integer' },

		enfermedad_trans: { collection: 'enfermedad_trans', via: 'medicinas' }
	},
	autoUpdatedAt: false
};

