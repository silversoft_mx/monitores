/**
* Signo_extra.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		nombre: { type: 'string', required: true },

		descripcion: { type: 'string' },

		enfermedad_trans: { model: 'enfermedad_trans' }
	},
	autoUpdatedAt: false

};

