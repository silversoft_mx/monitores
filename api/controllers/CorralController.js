/**
 * CorralController
 *
 * @description :: Server-side logic for managing corrals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	ver: function(req, res){
		Corral.find({}).exec(function findCB(err, results){
			return res.view('corral/ver', { 'corrales': results });
		});
	},

	registrar: function(req, res){

	}
};

