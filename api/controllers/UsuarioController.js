/**
 * UsuarioController
 *
 * @description :: Server-side logic for managing usuarios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var bcrypt = require('bcrypt');

module.exports = {

	entrar: function(req, res)
	{
		if(!req.param('email') || !req.param('password'))
		{
			req.flash('err', req.__("not-whitespaces"));
			res.redirect('/');
			return;
		}
		Usuario.findOne({correo: req.param('email')}).exec(function findOneCB(err, usuario){
			if(err)
				return next(err);
			if(!usuario)
			{
				req.flash('err', req.__("not-exists-user-email"));
				res.redirect('/');
				return;
			}
			bcrypt.compare(req.param('password'), usuario.contrasena, function(err, resp) {
				if(!resp)
				{
					req.flash('err', req.__("incorrect-password"));
					res.redirect('/');
					return;
				}
				usuario.fecha_ingreso = new Date();
				usuario.save(function(err, s){});
				
				req.session.authenticated = true;
				req.session.Usuario = usuario;
				res.redirect('/');
			});
		});
	},

	salir: function(req, res)
	{
		req.session.destroy();
		res.redirect('/');
	},

	registrar: function(req, res)
	{
		if(!req.param('nombre') || !req.param('correo') || !req.param('apellido') || 
			!req.param('contrasena') || !req.param('grupo'))
		{
			req.flash('err', req.__("not-whitespaces"));
			res.redirect('/registrar');
			return;
		}else if(req.param('nombre').length < 5){
			req.flash('err', req.__("user-minlength"));
			res.redirect('/registrar');
			return;
		}


		Usuario.create(req.body).exec(function createCB(err, created){
			if(err)
			{
				req.flash('err', req.__("error-unknown"));
				res.redirect('/registrar');
				return;
			}
			req.flash('success', req.__("user-created"));
			res.redirect('/usuario/ver');
		});
	}

};

