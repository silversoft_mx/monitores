/**
 * ResController
 *
 * @description :: Server-side logic for managing res
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	ver: function(req, res){
		Res.find({}).exec(function findCB(err, results){
			return res.view('res/ver', { 'reses': results });
		});
	},

	registrar: function(req, res){

		i_arete_siniiga = req.param('id_arete_siniiga');
		i_arete = req.param('id_arete');
		i_fecha_entrada = req.param('fecha_entrada');
		i_precio_compra = req.param('precio_compra');
		i_sexo = req.param('radioSexo');
		i_fecha_nacimiento = req.param('fecha_nacimiento');
		i_datos_adicionales = req.param('datos_adicionales');
		i_peso = req.param('peso');
		i_altura = req.param('altura');

		i_corral = req.param('corral');
		i_raza = req.param('raza');

		Res.create({
			arete_siniiga : i_arete_siniiga,
			fecha_ingreso : i_fecha_entrada,
			raza : i_raza,
			sexo : i_sexo,
			precio_compra : i_precio_compra,
			}).exec(function createCB(err, created){
  				console.log('Created user with id ' + created.arete_siniiga);
		});
		return res.ok();
	},

	alerta: function(req, res){
		Res.find({ id: req.param('id') }).populate('signos_vitales').populate('medidas').populate('alertas')
		.exec(function findCB(err, results){
			
			if(results.length == 0)
			{
				return res.redirect('/');
			}
			console.log(results);
			results = results[0];
			
			if(results.medidas.length > 0)
				results.medidas = results.medidas[results.length-1];
			else{
				results.medidas['peso'] = '';
				results.medidas['altura'] = '';
			}

			results['estado'] = results.alertas[results.alertas.length-1].estado;
			results['comentario'] = results.alertas[results.alertas.length-1].comentario;


			Res.find({}).populate('alertas')
			.exec(function findCB(err, reses){
				var alertas = [];
				reses.forEach(function(item){
					if(item.alertas.length > 0)
					{
						var aler = item.alertas.pop();
						aler['grupo'] = item.grupo;
						alertas.push( aler );
					}
				});
				return res.view('res/alerta', { 'res': results, 'alertas': alertas });
			});
		});
	},

	alerta_update: function(req, res){
		var hoy = new Date();
		hoy.setMinutes(hoy.getMinutes()+10);	
		Alerta.update({res: req.param('id')}, 
			{ 'estado': req.body.estados, 'comentario': req.body.comentarios, 'updatedAt': hoy })
		.exec(function afterwards(err, updated){});

		return res.redirect('/');
	},
}

/*
id_arete
id_arete_siniiga
corral
fecha_entrada
precio_compra
raza
radioSexo
fecha_nacimiento
datos_adicionales
peso
altura
*/