/**
 * Signo_vitalController
 *
 * @description :: Server-side logic for managing signo_vitals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var uuid = require('node-uuid');
var nodemailer = require('nodemailer');
module.exports = {
	
	recibe: function(req, res)
	{
		var ress, temperatura, frecuencia_cardiaca, frecuencia_respiratoria;
		ress = req.param('res');
		temperatura = req.param('temperatura');
		frecuencia_cardiaca = req.param('frecuencia_cardiaca');
		frecuencia_respiratoria = req.param('frecuencia_respiratoria');
		
		if(!ress || !temperatura || !frecuencia_cardiaca || !frecuencia_respiratoria)
		{
			res.ok();
			return;
		}

		
		Signo_vital.create({'res': ress, 'temperatura': temperatura, 'frecuencia_cardiaca': frecuencia_cardiaca, 
			'frecuencia_respiratoria': frecuencia_respiratoria, 'fecha': new Date()})
		.exec(function createCB(err, created){
			if(err){
				console.log(err);
				return;
			}
			Signo_vital.publishCreate(
				{ 
					'id': created.id,
					'temperatura': created.temperatura, 
					'frecuencia_respiratoria': created.frecuencia_respiratoria, 
					'frecuencia_cardiaca': created.frecuencia_cardiaca,
					'res': created.res
				}
			);
		});
		
		/*
		Signo_vital.publishCreate(
			{ 
				'id': uuid.v1(),
				'temperatura': temperatura, 
				'frecuencia_respiratoria': frecuencia_respiratoria, 
				'frecuencia_cardiaca': frecuencia_cardiaca,
				'res': ress
			}
		);
		*/

		var msj = ('La res #' + ress + ' debe ser atendida. <p>Temperatura actual es de ' + temperatura + 'C</p><p>Frecuencia Respiratoria: ' + frecuencia_respiratoria + 'RPM</p><p>Frecuencia Cardiaca: ' + frecuencia_cardiaca + 'LPM');

		var myQuery = Alerta.find();
		myQuery.where({ 'res': ress });
		myQuery.sort('createdAt DESC');
		myQuery.limit(1);
		
		var estado = 'falso';
		var fecha = new Date();
		var id;
		var hoy = new Date();
		var enviar = false;

		myQuery.exec(function callBack(err,results){
			var obj;
			while (results.length)
			{
				obj = results.pop();
				console.log('Found User with alert ' + obj.updatedAt)
				estado = obj.estado;
				fecha = new Date(obj.updatedAt.getTime());
				id = obj.id;
				console.log(fecha);
			}
			console.log('----pow----');

			if(frecuencia_cardiaca > 80 || frecuencia_cardiaca < 70 || frecuencia_respiratoria < 13 || frecuencia_respiratoria > 20 || temperatura > 38 || temperatura < 37)
			{
				if(estado.localeCompare('falso') == 0)
				{
					enviar = true;
					hoy.setMinutes(hoy.getMinutes()+3);
					Alerta.create({'res': ress, 'updatedAt': hoy}).exec(function createCB(err, created){});
				}else if(estado.localeCompare('checar') == 0 && hoy.getTime() > fecha.getTime()){
					enviar = true;
				}
			}

			if(estado.localeCompare('finalizado') == 0){
				//Alerta.destroy({res: req.param('id')}).exec(function deleteCB(err){});
				enviar = true;
				hoy.setMinutes(hoy.getMinutes()+3);
				Alerta.create({'res': ress, 'updatedAt': hoy}).exec(function createCB(err, created){});
			}
			

			if(enviar)
			{
				if(hoy.getTime() == fecha.getTime()){
					hoy.setMinutes(hoy.getMinutes()+3);
					Alerta.update({'res': ress}, {'updatedAt': hoy}).exec(function afterwards(err, updated){});
				}
				var transporter = nodemailer.createTransport({
					service: 'Gmail',
					auth: {
						user: 'crymore3@gmail.com',
						pass: 'stephanie000.'
					}
				});
				var mailOptions = {
					from: 'Victor <victor@dreamsoft.st>',
					to: 'victor.riverac92@gmail.com, jorgerguez92@gmail.com',
					subject: 'MonitoRes ~ Notificación Alerta',
					text: msj,
					html: msj
				};
				transporter.sendMail(mailOptions, function(error, info){});

				var r = Res.find().populate('alertas');
				r.where({'id' : ress});
				r.limit(1);
				r.exec(function callBack(err, results){
					var obj = results.pop();
					if(obj.alertas.length > 0){
						obj['estado'] = obj.alertas[obj.alertas.length-1].estado;
					}else{
						obj['estado'] = '';
					}
					Alerta.publishCreate(
						{ 
							'id': uuid.v1(),
							'temperatura': temperatura, 
							'corral': obj.grupo,
							'res': ress,
							'estado': obj['estado']
						}
					);

				});
				
			}




		});
		

		res.ok();
	}

};

