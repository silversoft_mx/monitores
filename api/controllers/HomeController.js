/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
	home: function(req, res)
	{
		if(req.isSocket)
		{
			Signo_vital.watch(req.socket);
			Alerta.watch(req.socket);
		}else{
			Res.find({}).populate('signos_vitales').populate('medidas').populate('alertas')
			.exec(function findCB(err, reses){
				if (err)
				{ 
					return res.serverError(err);
				}
				var reses2 = [];
				var alertas = [];
				reses.forEach(function(item){
					if(item.medidas.length > 0){
						item.medidas = item.medidas[item.medidas.length-1];
					}else{
						item.medidas.peso = '';
						item.medidas.altura = '';
					}

					if(item.signos_vitales.length > 0){
						item['signos'] = item.signos_vitales.pop();
					}else{
						item['signos'] = { 
							'temperatura': '', 'frecuencia_respiratoria': '',
							'frecuencia_cardiaca': ''
						};
					}
					if(item.alertas.length > 0){
						item['estado'] = item.alertas[item.alertas.length-1].estado;
					}else{
						item['estado'] = '';
					}
					console.log(item);

					reses2.push(item);
					if(item.alertas.length > 0)
					{
						var aler = item.alertas.pop();
						aler['grupo'] = item.grupo;
						alertas.push( aler );
					}
				});
				return res.view('inicio', { 'reses': reses2, 'alertas': alertas });
				
			});
		}
	}

};

