/**
 * isAdmin
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user and is admin
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

	if (req.session.authenticated) {
		if(req.session.Usuario.grupo.nombre == "Admin")
		{
			return next();
		}
		return res.forbidden(req.__("forbidden"));
	}

	return res.forbidden(req.__("forbidden"));
};
