/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'get /': 'HomeController.home',

  //Registro Nuevo Usuario
  'get /usuario/registrar': { view: 'usuario/registrar' },
  'post /usuario/registrar': 'UsuarioController.registrar',
  
  //Login
  'get /entrar': { view: 'usuario/entrar', locals: { layout: '' } },
  'post /entrar': 'UsuarioController.entrar',
  '/salir': 'UsuarioController.salir',

  // Reses
  '/reses': 'ResController.ver',
  'get /res/registrar': { view: 'res/registrar' },
  'post /res/registrar': 'ResController.registrar',

  // Corrales
  '/corrales': 'CorralController.ver',
  'get /corral/registrar': { view: 'corral/registrar' },
  'post /corral/registrar': 'CorralController.registrar',

  // Signo_vital
  'post /signo_vital': 'Signo_vitalController.recibe',

  //Registro Nuevo Usuario
  'get /res/alerta/:id': 'ResController.alerta',
  'post /res/alerta/:id': 'ResController.alerta_update',





  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
