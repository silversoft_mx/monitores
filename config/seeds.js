/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */
 
module.exports.seeds = {
	rol: {
		data: [
			{
				id: 1,
				nombre: 'Admin',
				descripcion: 'Tiene acceso a todo el sistema'
			},
			{
				id: 2,
				nombre: 'Gerente',
				descripcion: 'Tiene acceso a todo el sistema'
			},
			{
				id: 3,
				nombre: 'Medico',
				descripcion: 'Tiene acceso a las reses'
			},
			{
				id: 4,
				nombre: 'Alimentacion',
				descripcion: 'Tiene acceso a los alimentos y formulas de corrales'
			},
		],
		overwrite: false
	},
	usuario: {
		data: [
			{
				nombre: 'Victor',
				apellido: 'Carrillo',
				correo: 'victor.riverac92@gmail.com',
				contrasena: '123456',
				rol: 1
			},
			{
				nombre: 'Jorge',
				apellido: 'Rodriguez',
				correo: 'jorgerguez92@gmail.com',
				contrasena: '123456',
				rol: 1
			},
			{
				nombre: 'Alberto',
				apellido: 'Leon',
				correo: 'alberto.leonlopez@gmail.com',
				contrasena: '123456',
				rol: 1
			}
		],
		overwrite: false
	},
	arete: {
		overwrite: false
	},
	comentario_enfermedad: {
		overwrite: false
	},
	corral: {
		overwrite: false
	},
	enfermedad: {
		overwrite: false
	},
	enfermedad_trans: {
		overwrite: false
	},
	estado: {
		overwrite: false
	},
	estado_enfermedad: {
		overwrite: false
	},
	formula: {
		overwrite: false
	},
	formula_corral: {
		overwrite: false
	},
	grupo: {
		overwrite: false
	},
	medicina: {
		overwrite: false
	},
	medida: {
		overwrite: false
	},
	proveedor: {
		overwrite: false
	},
	raza: {
		overwrite: false
	},
	receptor: {
		overwrite: false
	},
	res: {
		overwrite: false
	},
	signo_corral: {
		overwrite: false
	},
	signo_enfermedad: {
		overwrite: false
	},
	signo_extra: {
		overwrite: false
	},
	signo_vital: {
		overwrite: false
	}
}